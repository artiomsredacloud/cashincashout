<?php

namespace Cashincashout\Helpers;

interface CustomExceptionService
{
    public function isValidPathToCSVFile(string $value): void;
}

class CustomExceptionHelper implements CustomExceptionService
{
    public function isValidPathToCSVFile(string $value): void
    {
        if (!file_exists($value) && !strstr($value, ".csv")) {
            throw new \InvalidArgumentException(
                sprintf(
                    'CSV file "%s" not found.', $value
                )
            );
        } else {
            try {
                $file = file_get_contents($value);
            } catch (\Exception $e) {
                throw new \InvalidArgumentException(
                    sprintf(
                        $e . 'Cant get file content "%s".', $value
                    )
                );
            }
        }
    }


}